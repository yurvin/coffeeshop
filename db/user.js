/**
 * Created by vini on 14.05.16.
 */

var userDb = {};

var mongoose = require('mongoose');

var config = require('../config/config.json');

var db_name = 'users';
var host = config.host;

var db = mongoose.connection;

db.userParams = {};

db.on('error', console.error);
db.once('open', function() {
    // Здесь создаём схемы и модели.
    console.log('db open');

    //schema user
    userDb.userSchema = new mongoose.Schema({
        "firstName" : String,
        "patronymic" : String,
        "surname" : String,
        "telCountryCode": String,
        "telOperatorCode": Number,
        "phoneNumber": Number,
        "login": String,
        "email" : String,
        "password": String,
        "active" : Boolean,
        "admin" : Boolean,
        "manager" : Boolean,
        "count_purchased" : Number,
        "in_basket" : [String],
        "vip" : Boolean,
        "registration_date": Date
    });

    userDb.User = mongoose.model('User', userDb.userSchema);

});

//create
userDb.createUser = function (userParams) {
    var user = new userDb.User(userParams);

    user.save(function(err, user) {
        if (err) return console.error(err);
        console.dir(user);
    });
};

//get
userDb.getUser = function (userParams) {
    
    var user = this.User.findOne(userParams, function (err, user) {

        if (err) return console.error(err);
        
        return user;
    });
    
    return user;
};

mongoose.connect('mongodb://'+host+'/'+db_name);

module.exports = userDb;