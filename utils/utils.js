/**
 * Created by vini on 14.05.16.
 */


/**
 *Метод расширения одного объекта другим (поверхностное копирование)
 * @param child
 * @param parent
 * @param rewrite
 */
this.extend = function(child, parent, rewrite) {
    rewrite = rewrite || false;
    var i;
    child = child || {};
    for (i in parent) {
        if (parent.hasOwnProperty(i)) {
            if (rewrite) {
                child[i] = parent[i];
            } else {
                if(typeof child[i] == 'undefined') {
                    child[i] = parent[i];
                }
            }
        }
    }
    return child;
};

/**
 * Метод расширения одного объекта другим (глубокое копирование)
 * @param child
 * @param parent
 * @param rewrite
 * @returns {{}}
 */
this.extendDeep = function(child, parent, rewrite) {
    rewrite = rewrite || false;
    var i;
    var toStr = Object.prototype.toString;
    var astr = "[object Array]";
    child = child || {};
    for (i in parent) {
        if (parent.hasOwnProperty(i)) {
            if (parent[i] !== null && typeof parent[i] === "object") {
                child[i] = (toStr.call(parent[i]) === astr) ? [] : {};
                this.extendDeep(child[i], parent[i], rewrite);
            } else {
                if (rewrite) {
                    child[i] = parent[i];
                } else {
                    if(typeof child[i] == 'undefined') {
                        child[i] = parent[i];
                    }
                }
            }
        }
    }
    return child;
};

/**
 * Проверка типа на массив
 * @param arr
 * @returns {boolean}
 */
this.isArray = function (arr) {
    var isArray = (Object.prototype.toString.call(arr) == '[object Array]') ? true : false;
    return isArray;
};

/**
 * Конвертация rgb в hex
 * @param rgbColor
 * @returns {string}
 */
this.rgbToHex = function (rgbColor) {
    rgbColor = rgbColor.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgbColor[1]) + hex(rgbColor[2]) + hex(rgbColor[3]);
};

/**
 * Из коллекции получить массив по ключу
 * @param coll
 * @param key
 * @returns array
 */
this.coll2Array = function (coll, key) {
    if(!coll || !key) return null;

    var arr = [];

    coll.forEach(function (el) {
        arr.push(el[key]);
    });

    return arr
};

/**
 * Генерация случайного id
 * @returns {string}
 */
this.createId = function () {
    return '_' + Math.random().toString(36).substr(2, 9);
};

/**
 * Проверка что ввел пользователь email/login
 * Если email то возврат
 * Иначе смена ключа на login и возврат
 * @param params
 * @returns {*}
 */
this.checkEmail = function (params) {
    var sobaka = params.email.indexOf('@');

    if(sobaka != -1) return params;

    params.login = params.email;
    delete params.email;

    return params;
};