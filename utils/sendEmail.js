/**
 * Created by vini on 14.05.16.
 */

var sendEmail = {};

var nodemailer = require('nodemailer');

// create reusable transporter object using the default SMTP transport
//support@coffee-live.ru
var transporter = nodemailer.createTransport('smtps://support%40coffee-live.ru:vini9874@smtp.mail.ru');

sendEmail.send = function (sendParams) {

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: '"coffee-live.ru" <support@coffee-live.ru>', // sender address
        // to: 'vini1.ru@mail.ru', // list of receivers
        to: sendParams.to.join(), // list of receivers
        subject: sendParams.subject, // Subject line
        text: sendParams.text, // plaintext body
        html: sendParams.html // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
    });
};

module.exports = sendEmail;