/**
 * Created by vini on 18.05.16.
 */
var utils = require('./utils');

this.createGuest = function () {
    
    var guestObj = {
        in_basket: [],
        guestId: utils.createId(),
        firstName: 'Гость',
        "visiting": new Date()
    };

    return guestObj;
};