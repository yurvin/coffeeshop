/**
 * Created by vini on 15.05.16.
 */

var express = require('express');
var router = express.Router();
var utilsUser = require('../utils/utilsUser');

/* GET home page. */
router.get('/', function(req, res, next) {

    var user = req.session.user ? req.session.user : utilsUser.createGuest();
    res.render('gds', { user: user });
    
});

router.post('/', function (req, res, next) {

    if(req.body['login'] == ''){
        res.redirect('/auth');
    } else {
        req.session.destroy(function(err){
            console.log('session.destroy error', err);
            res.render('gds', { user:  utilsUser.createGuest() });
        });
    }

});

module.exports = router;