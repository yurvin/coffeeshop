/**
 * Created by vini on 10.05.16.
 */

var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var utils = require('./../utils/utils');
var _ = require('underscore');
var deferred = require('deferred'); //нужно из-за асинхронного запроса в db

var userDb = require('../db/user');
var sendEmail = require('./../utils/sendEmail');
var User = require('../db/user');

var registrationParams = {
    "firstName" : null,
    "patronymic" : null,
    "surname" : null,
    "telCountryCode": null,
    "telOperatorCode": null,
    "phoneNumber": null,
    "email" : null,
    "password": null
};

/* GET registration page. */

router.use('/', bodyParser.urlencoded({
    extended: true
}));

router.get('/', function(req, res, next) {
    res.render('registration', { registrationParams: registrationParams });
});

router.post('/', function (req, res, next) {

    var validForm = validationForm(req.body);

    if(validForm.valid){
        registrationParams = extendRegistrationParams(req.body);
        var getUser = User.getUser({email: registrationParams.email});


        deferred(getUser)(function (user) {
            if (user) {
                registrationParams = {dubl: true};
                res.render('registration', {registrationParams: registrationParams});
                return;
            } else {
                registrationParams.dubl = false;
            }

            userDb.createUser(registrationParams);

            var text =
                'Вы (или кто-то, используя Ваше имя электронной почты) зарегистрировали логин ' +
                '<b>' +
                '<a href="/compose?To=vini1.ru@mail.ru" rel="noopener">' + registrationParams.email + '</a>' +
                '</b> ' +
                'для доступа в личный кабинет на сайте ' +
                '<a href="http://www.coffee-live.ru" target="_blank" rel="noopener">www.coffee-live.ru</a>.' +
                '<br><br>\nЕсли это действие было совершено без Вашего ведома и желания, пожалуйста, ' +
                'сообщите об этом администратору сервиса ' +
                '<a href="//e.mail.ru/compose/?mailto=mailto%3asupport@coffee-live.ru" target="_blank" rel="noopener">support@coffee-live.ru</a><br>' +
                '<br>Указанные данные регистрации:<br>' +
                '<table>' +
                '<tbody>' +
                '<tr>' +
                '<td style="color: red;text-align: right;">Ваш e-mail: </td>' +
                '<td>' + registrationParams.email + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td style="color: red;text-align: right;">Ваш логин: </td>' +
                '<td>' + registrationParams.login + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td style="color: red;text-align: right;">Ваш пароль: </td>' +
                '<td>' + registrationParams.password + '</td>' +
                '</tr>' +
                '</tbody>' +
                '</table>' +
                'Письмо создано автоматической системой оповещения ' + new Date().getDate() + '.' + new Date().getMonth() + '.' + new Date().getFullYear() + '</i></div>';

            var mailOptions = {
                to: [registrationParams.email],
                subject: 'Регистрация на coffee-live.ru',
                text: '',
                html: text
            };

            sendEmail.send(mailOptions);

            res.redirect('/registration-success');
        });
    } else {

        for (var key in validForm){
            var elValid = validForm[key];

            if(key == 'valid') continue;

            req.body[key] = elValid ? req.body[key] : elValid;
        }

        res.render('registration', { registrationParams: req.body });
    }
});

var validationForm = function (registrationParams) {
    var result = {};

    var regStr = new RegExp('^[а-яА-ЯёЁa-zA-Z0-9]+$'); //Набор из букв и цифр (латиница + кириллица)
    var regNumber = new RegExp('[0-9]'); // только цифры
    var regEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i; //email

    //Пароль (Строчные и прописные латинские буквы, цифры, спецсимволы. Минимум 4 символа):
    var regPassword = new RegExp('(?=^.{4,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$');

    for(var key in registrationParams){
        result[key] = (registrationParams[key] !== null) ? true : false;
        if(!result[key]) continue;

        switch (key){
            case 'firstName':
            case 'patronymic':
            case 'surname':
                result[key] = (regStr.test(registrationParams[key]) || registrationParams[key] == '') ? true : false;
                break;

            case 'email':
                result[key] = (regEmail.test(registrationParams[key]) ) ? true : false;
                break;
            
            case 'password':
                result[key] = (regStr.test(registrationParams[key])) ? true : false;
                break;

            case 'telOperatorCode':
            case 'phoneNumber':
                result[key] = (regNumber.test(registrationParams[key])  || registrationParams[key] == '') ? true : false;
                break;
        }
    }

    var values = _.values(result);

    for(var i = 0; i<values.length; i++){

        if(!values[i]){
            result['valid'] = false;
            break;
        } else {
            result['valid'] = true;
        }
    }

    return result;
};

var extendRegistrationParams = function (registrationParams) {
   
    var endOfLogin = registrationParams.email.indexOf('@');
   
    var extendParams = {
        "active" : false,
        "admin" : false,
        "manager" : false,
        "count_purchased" : 0.0,
        "in_basket" : [], //список id товаров в корзине
        "vip" : false,
        "registration_date": new Date(),
        "telCountryCode": '+7',
        "login": registrationParams.email.substring(0, endOfLogin)
    };

    registrationParams = utils.extend(registrationParams, extendParams, true);

    return registrationParams;
};

module.exports = router;