/**
 * Created by vini on 21.05.16.
 */

var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

router.use('/', bodyParser.urlencoded({
    extended: true
}));

router.get('/', function(req, res, next) {
    res.render('registration-success');
});



module.exports = router;