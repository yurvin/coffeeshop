/**
 * Created by vini on 11.05.16.
 */

var express = require('express');
var router = express.Router();
var User = require('../db/user');
var deferred = require('deferred'); //нужно из-за асинхронного запроса в db

var utils = require('./../utils/utils');

var authorizationParams = {
    email: null,
    password: null,
    register: true
};

/* GET registration page. */
router.get('/', function(req, res, next) {
    res.render('authorization', { authorizationParams: authorizationParams });
});

router.post('/', function (req, res, next) {
    var userParams = utils.checkEmail(req.body);

    var getUser = User.getUser(userParams);

    deferred(getUser)(function (user) {

        if(!user){
            authorizationParams.register = false;
            res.render('authorization', { authorizationParams: authorizationParams });
        } else {

            req.session.user  = {
                email: user.email,
                in_basket : user.in_basket,
                _id : user._id,
                surname : user.surname,
                firstName : user.firstName,
                patronymic : user.patronymic
            };

            res.redirect('/gds');
        }

    });
});

module.exports = router;